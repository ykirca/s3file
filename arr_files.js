var arrBucketName = "yagmurs3test";
var bucketRegion = "eu-central-1";
var IdentityPoolId = "eu-central-1:5fd09a82-a0fb-47ef-b90d-aebef4cba859";

AWS.config.update({
  region: bucketRegion,
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: IdentityPoolId
  })
});

var s3 = new AWS.S3({
  apiVersion: "2012-10-17",
  params: { Bucket: arrBucketName }
});


function listarrs() {
  s3.listObjects({ Delimiter: "/" }, function(err, data) {
    if (err) {
      return alert("There was an error listing your ARRs: " + err.message);
    } else {
      var arrs = data.CommonPrefixes.map(function(commonPrefix) {
        var prefix = commonPrefix.Prefix;
        var arrName = decodeURIComponent(prefix.replace("/", ""));
        return getHtml([
          "<li>",
          "<span style=\"background-color:red;font-weight:bold;\" onclick=\"deletearr('" + arrName + "')\">X</span>",
          "<span style=\"font-weight:bold;\" onclick=\"viewarr('" + arrName + "')\">",
          arrName,
          "</span>",
          "</li>"
        ]);
      });
      var message = arrs.length
        ? getHtml([
            "<p>Click on an ARR name to view it.</p>",
            "<p>Click on the X to delete the ARR.</p>"
          ])
        : "<p>You do not have any ARRs. Please Create ARR.";
      var htmlTemplate = [
        "<h2>ARRs</h2>",
        message,
        "<ul>",
        getHtml(arrs),
        "</ul>",
        "<button onclick=\"createarr(prompt('Enter ARR Name:'))\">",
        "Create New ARR",
        "</button>"
      ];
      document.getElementById("app").innerHTML = getHtml(htmlTemplate);
    }
  });
}


function createarr(arrName) {
  arrName = arrName.trim();
  if (!arrName) {
    return alert("ARR names must contain at least one non-space character.");
  }
  if (arrName.indexOf("/") !== -1) {
    return alert("ARR names cannot contain slashes.");
  }
  var arrKey = encodeURIComponent(arrName);
  s3.headObject({ Key: arrKey }, function(err, data) {
    if (!err) {
      return alert("ARR already exists.");
    }
    if (err.code !== "NotFound") {
      return alert("There was an error creating your ARR: " + err.message);
    }
    s3.putObject({ Key: arrKey }, function(err, data) {
      if (err) {
        return alert("There was an error creating your ARR: " + err.message);
      }
      alert("Successfully created ARR.");
      viewarr(arrName);
    });
  });
}


function viewarr(arrName) {
  var arrarr_filesKey = encodeURIComponent(arrName) + "/";
  s3.listObjects({ Prefix: arrarr_filesKey }, function(err, data) {
    if (err) {
      return alert("There was an error viewing your ARR: " + err.message);
    }
    // 'this' references the AWS.Response instance that represents the response
    var href = this.request.httpRequest.endpoint.href;
    var bucketUrl = href + arrBucketName + "/";

    var arr_files = data.Contents.map(function(arr_file) {
      var arr_fileKey = arr_file.Key;
      var arr_fileUrl = bucketUrl + encodeURIComponent(arr_fileKey);
      return getHtml([
        "<span>",
        "<div>",
        '<a href="'+arr_fileUrl+'" download="'+arr_fileKey+'"/>',
        "</div>",
        "<div>",
        "<span onclick=\"deletearr_file('" +
          arrName +
          "','" +
          arr_fileKey +
          "')\">",
        "X",
        "</span>",
        "<span>",
        arr_fileKey.replace(arrarr_filesKey, ""),
        "</span>",
        "</div>",
        "</span>"
      ]);
    });
    var message = arr_files.length
      ? "<p>Click on the X to delete the ARR file</p>"
      : "<p>You do not have any arr_files in this ARR. Please add ARR file.</p>";
    var htmlTemplate = [
      "<head>",
      "<style>",
      "table, th, td { border: 1px solid black;}",
      "</style>",
      "</head>",
      "<body>",
        "<h1>",
          "Project Name: " + arrName,
        "</h1>",
        "<table style=\"width:600px\" cellpadding=\"10\">",
          "<tr>",
            "<th>Project Name</th>",
            "<th>Team</th>",
            "<th>Owner Email</th>",
            "<th>2Pager Link</th>",
            "<th>Approval Status</th>",
            "<th>Data Classification</th>",
            "<th>Expected AWS Resources</th>",
          "</tr>",
          "<tr>",
            "<td>"
            + arrName,
            "</td>",
            "<td>Infra</td>",
            "<td>yagmur.bayansal@booking.com</td>",
            "<td>",
            getHtml(arr_files),
            "</td>",
            "<td>In Progress</td>",
            "<td>PII</td>",
            "<td>EC2,RDS,S3</td>",
          "</tr>",
        "</table>",
       "</body>",

      message,
      '<input id="arr_fileupload" type="file" accept="image/*">',
      '<button id="addarr_file" onclick="addarr_file(\'' + arrName + "')\">",
      "Add ARR file",
      "</button>",
      '<button onclick="listarrs()">',
      "Back To ARRs",
      "</button>"
    ];
    document.getElementById("app").innerHTML = getHtml(htmlTemplate);
  });
}


function addarr_file(arrName) {
  var files = document.getElementById("arr_fileupload").files;
  if (!files.length) {
    return alert("Please choose a file to upload first.");
  }
  var file = files[0];
  var fileName = file.name;
  var arrarr_filesKey = encodeURIComponent(arrName) + "/";

  var arr_fileKey = arrarr_filesKey + fileName;

  // Use S3 ManagedUpload class as it supports multipart uploads
  var upload = new AWS.S3.ManagedUpload({
    params: {
      Bucket: arrBucketName,
      Key: arr_fileKey,
      Body: file
    }
  });

  var promise = upload.promise();

  promise.then(
    function(data) {
      alert("Successfully uploaded ARR file.");
      viewarr(arrName);
    },
    function(err) {
      return alert("There was an error uploading your ARR file: ", err.message);
    }
  );
}


function deletearr_file(arrName, arr_fileKey) {
  s3.deleteObject({ Key: arr_fileKey }, function(err, data) {
    if (err) {
      return alert("There was an error deleting your ARR file: ", err.message);
    }
    alert("Successfully deleted ARR file.");
    viewarr(arrName);
  });
}


function deletearr(arrName) {
  var arrKey = encodeURIComponent(arrName) + "/";
  s3.listObjects({ Prefix: arrKey }, function(err, data) {
    if (err) {
      return alert("There was an error deleting your ARR: ", err.message);
    }
    var objects = data.Contents.map(function(object) {
      return { Key: object.Key };
    });
    s3.deleteObjects(
      {
        Delete: { Objects: objects, Quiet: true }
      },
      function(err, data) {
        if (err) {
          return alert("There was an error deleting your ARR: ", err.message);
        }
        alert("Successfully deleted ARR.");
        listarrs();
      }
    );
  });
}
